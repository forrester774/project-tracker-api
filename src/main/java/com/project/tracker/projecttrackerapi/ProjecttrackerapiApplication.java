package com.project.tracker.projecttrackerapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProjecttrackerapiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjecttrackerapiApplication.class, args);
    }

}

